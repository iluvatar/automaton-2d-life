#!/bin/bash

for a in $1/*.dat; do
    echo "Processing $a"
    cat <<EOF > $1/tmp.gp
set size square
unset key
unset colorbox
unset xtics
unset ytics 
set cbrange [0:1]
set yrange [] reverse
set view map
set term png transparent enhanced truecolor giant; 
set out '${a}.png'; 
splot '$a' matrix w image
EOF
gnuplot $1/tmp.gp
done
