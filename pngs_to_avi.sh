#!/bin/bash

#if [ "$#" -ne "1" ]; then
#    echo "ERROR"
#    echo "Usage : $0 DirWithPngFiles"
#    exit 1;
#fi

INPUT_DIR=$1
#INPUT_DIR="DISPLAY/"

# fps controla los frames per second 
CODEC=mpeg4
#CODEC=mpeg4v2 # Windows ready
#CODEC=mpeg2video # Windows ready

mencoder "mf://${INPUT_DIR}/*.png" -mf fps=20 -o ${INPUT_DIR}/output.avi -ovc lavc -lavcopts vcodec=$CODEC 

echo -e " \033[1;34m Video saved in ${INPUT_DIR}/output.avi  \033[0m "
echo -e " \033[1;34m You can reproduce your video with the command : \033[0m  "
echo -e " \033[1;34m mplayer ${INPUT_DIR}/output.avi  \033[0m "
