#include <vector>
#include <cmath>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <sstream>
#include <string>
#include <iomanip>

//--------------------------------------------------------------
// typedefs
typedef std::vector<std::vector<int>> grid_t;

//--------------------------------------------------------------
// global vars
const int N = 17;
const int NCLICKS = 250;
enum STATES {RANDOM = 0, STATIC1, STATIC2, GLIDER, PERIODIC1};


//--------------------------------------------------------------
// function declaration
void initialize(grid_t & ca, const STATES & state);
void reset(grid_t & ca);
void evolve(grid_t & ca);
void print(const grid_t & ca, const std::string & filename);

//--------------------------------------------------------------
// main
int main(int argc, char **argv)
{
  srand48(10);
  grid_t ca; 
  std::stringstream ss; 

  // declare automata
  ca.resize(N); 
  for (auto & vec : ca) { vec.resize(N, false); }

  // initilize automata
  initialize(ca, PERIODIC1);

  // evolve
  for(long click = 0; click < NCLICKS; ++click) {
    std::cout << "click = " << click << std::endl;
    evolve(ca);
    ss.str(""); ss.fill('0');
    ss << "ca-" << std::setw(4) << click << ".dat" ;
    print(ca, ss.str());
  }

  // print
  //print(ca, 'final-ca.txt');

  return 0;
}


//--------------------------------------------------------------
// function definition
void initialize(grid_t & ca, const STATES & state)
{
  // the last one written and uncommented is the working initial condition
  switch (state) {
  case RANDOM:
    int ii, jj;
    for (ii = 0; ii < ca.size(); ++ii) {
      for (jj = 0; jj < ca[ii].size(); ++jj) {
	ca[ii][jj] = lrint(drand48());
      }
    };
    break;
  case STATIC1:
    assert(6 == N);
    reset(ca);
    ca[1][2] = ca[1][3] = 1;
    ca[2][1] = ca[2][4] = 1;
    ca[3][1] = ca[3][4] = 1;
    ca[4][2] = ca[4][3] = 1;
    break;
  case STATIC2:
    assert(6 == N);
    reset(ca);
    ca[1][3] = 1;
    ca[2][2] = ca[2][4] = 1;
    ca[3][1] = ca[3][4] = 1;
    ca[4][2] = ca[4][3] = 1;
    break;
  case GLIDER:    
    assert(6 <= N);
    reset(ca);
    ca[1][3] = 1;
    ca[2][1] = ca[2][3] = 1;
    ca[3][2] = ca[3][3] = 1;
    break;
  case PERIODIC1:    
    assert(17 <= N);
    reset(ca);
    int i;
    i = 2; ca[i][4] = ca[i][5] = ca[i][6] = ca[i][10] = ca[i][11] = ca[i][12] = 1;
    for (i = 4; i <= 6; ++i) { ca[i][2] = ca[i][7] = ca[i][9] = ca[i][14] = 1; }
    i = 7; ca[i][4] = ca[i][5] = ca[i][6] = ca[i][10] = ca[i][11] = ca[i][12] = 1;
    i = 9; ca[i][4] = ca[i][5] = ca[i][6] = ca[i][10] = ca[i][11] = ca[i][12] = 1;
    for (i = 10; i <= 12; ++i) { ca[i][2] = ca[i][7] = ca[i][9] = ca[i][14] = 1; }
    i = 14; ca[i][4] = ca[i][5] = ca[i][6] = ca[i][10] = ca[i][11] = ca[i][12] = 1;
    break;
  default:
    std::cerr << "ERROR: Bad initial condition selected" << std::endl;
    std::exit(1);
  }
}

void reset(grid_t & ca)
{
  int ii, jj;
  for (ii = 0; ii < ca.size(); ++ii) {
    for (jj = 0; jj < ca[ii].size(); ++jj) {
      ca[ii][jj] = 0;
    }
  }
}

void evolve(grid_t & ca)
{
  static grid_t ca_old;
  ca_old = ca;
  const int size = ca.size();
  int ii, jj;
  for (ii = 0; ii < size; ++ii) {
    for (jj = 0; jj < size; ++jj) {
      // WARNING : this assumes states are just 0 or 1
      int count = ca_old[(ii + 1)%size][jj] + ca_old[(ii + size - 1)%size][jj] + // E + W 
	ca_old[ii][(jj + 1)%size] + ca_old[ii][(jj + size - 1)%size] + // N + S
	ca_old[(ii + 1)%size][(jj + 1)%size] + ca_old[(ii + size - 1)%size][(jj + 1)%size] + // NE + NW
	ca_old[(ii + size - 1)%size][(jj + size - 1)%size] + ca_old[(ii + 1)%size][(jj + size - 1)%size] ; // SW + SE
      // check new state
      if (1 == ca[ii][jj]) {
	if (count < 2) ca[ii][jj] = 0;
	else if (count > 3) ca[ii][jj] = 0;
      }
      else if (0 == ca[ii][jj]) {
	if (count == 3) ca[ii][jj] = 1;
      }
    }
  }
}
 
void print(const grid_t & ca, const std::string & filename)
{
  std::ofstream fout(filename);
  int ii, jj;
  for (ii = 0; ii < ca.size(); ++ii) {
    for (jj = 0; jj < ca[ii].size(); ++jj) {
      fout << ca[ii][jj] << " " ;
      std::cout << ca[ii][jj] << " " ;
    }
    fout << std::endl;
    std::cout << std::endl;
  }
  fout << std::endl;
  std::cout << std::endl;
}

